<?php
include "File.php";

class Repo
{

    private $files = array();


    public function __construct()
    {
        $this->files = array(new File('Opera',5 , ".txt"),
            new File("Kursach", 99999,".doc"),
            new File("home-video", 13,".mp5"),
            new File("Photo", 61,".jpg"),
        );
    }

    public function addNewFile($name, $size, $type_file)
    {
        if(isset($name) && isset($size) && isset($type_file))
        {
            $this->files[] = new File($name, $size, $type_file);
            return true;
        }
        return false;

    }

    private function fingByName($name)
    {
        if(isset($name))
        {
            for($i = 0; $i < count($this->files); $i++)
            {

                if($this->files[$i]->getName() == $name)
                {
                    return $i;
                }

            }

            return false;

        }
    }

    public function getAllFile()
    {
        return $this->files;
    }

    public function showAllDataofFile()
    {
        foreach ($this->files as $file)
            {
                print (
                    "File name - " . $file->getFileName() . "\n" .
                    "File size - " . $file->getSize() . "\n" .
                    "File type - " . $file->getTypeFile() . "\n"
                );
                $this->printdelimetr();
            }
    }


    public function showCountOfType($file_type)
    {
        if(isset($file_type))
        {
            $coutn_type = 0;
            foreach ($this->files as $file)
            {
                if($file->getTypeFile() == $file_type)
                    $coutn_type+= 1;

            }

            print ("File who == " . $file_type . " is -> " . $coutn_type . "\n");
            return $coutn_type;

        }

        return false;

    }


    public function showFileOfSize($size)
    {

        if(isset($size))
        {
            $coutn_file = 0;
            foreach ($this->files as $file)
            {
                if($file->getSize() >= $size)
                    $coutn_file+= 1;

            }

            print ("File who >= " . $size . " is -> " . $coutn_file . "\n");
            return $coutn_file;

        }

        return false;


    }




    public function printdelimetr()
    {
        print("****************************************************************\n");
    }


}