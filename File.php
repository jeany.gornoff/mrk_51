<?php

class File
{
    private $file_name;
    private $size;
    private $type_file;

    /**
     * Package constructor.
     * @param $name
     * @param $size
     * @param $type_file
     */
    public function __construct($file_name, $size, $type_file)
    {
        $this->file_name = $file_name;
        $this->size = $size;
        $this->type_file = $type_file;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->file_name;
    }

    /**
     * @param mixed $file_name
     */
    public function setFileName($file_name)
    {
        $this->file_name = $file_name;
    }


    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getTypeFile()
    {
        return $this->type_file;
    }

    /**
     * @param mixed $type_file
     */
    public function setTypeFile($type_file)
    {
        $this->type_file = $type_file;
    }




}